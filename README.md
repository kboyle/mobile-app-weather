# Mobile App Development Demo

This repository contains daily weather data for Saint John, NB (EnvCanada-2018-daily-weather-data.csv) obtained from Environment Canada and converted to a JSON structure (EnvCanada-2018-daily-weather-data.json).

Develop a visual interactive mobile application which does the following:

- Contains the EnvCanada-2018-daily-weather-data.json weather data.
- Generates a summary for each month containing the following aggregations:
    - Minimum Temperature (MIN of Min Temp (°C))
    - Average Temperature (AVERAGE of Mean Temp (°C))
    - Maximum Temperature (MAX of Max Temp (°C))
    - Total Precipitation (SUM of Total Precip (mm))
- Contains two views
    - Text View - presents the above monthly summary data as text.
    - Graphical View - presents the above monthly summary data graphically, with the months on the x-axis.

Provide the project source / structure, documentation on how to build the program and an explanation on why you chose the tools/libraries used. Keep in mind that the purpose of this exercise is not solely to provide a working solution. We are looking for candidates to demonstrate the principles and practices they feel are important in software engineering: dependency management, testing, project structure, data modelling, etc. 

### Submission
Email your response.
